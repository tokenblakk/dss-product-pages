﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Diet
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Visible = false;
            if (!IsPostBack)
            {
                imagebindGrid();
            }
        }
        private void Imageupload()
        {
            if (FileUpload1.HasFile)
            {
                int imagefilelength = FileUpload1.PostedFile.ContentLength;
                byte[] imgarray = new byte[imagefilelength];
                HttpPostedFile image = FileUpload1.PostedFile;
                image.InputStream.Read(imgarray, 0, imagefilelength);
                connection();
                query = "Insert into ProductImg (ImageName,Image) values (@Name,@Image)";
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@Name", SqlDbType.VarChar).Value = TextBox3.Text;
                com.Parameters.AddWithValue("@Image", SqlDbType.Image).Value = imgarray;
                com.ExecuteNonQuery();
                Label1.Visible = true;
                Label1.Text = "Image has been uploaded successfully";
                    
                imagebindGrid();

            }
        }

        protected void upload(object sender, EventArgs e)
        {
            Imageupload();
        }

        public void imagebindGrid()
        {
            connection();
            query = "Select id, ImageName,Image from ProductImg";
            SqlCommand com = new SqlCommand(query,con);
            SqlDataReader dr = com.ExecuteReader();
            GridView1.DataSource = dr;
            GridView1.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSSSite.App_Code;

namespace DSSSite
{
    public partial class ViewCart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                decimal cartTotal = 0;
                ShoppingCart usersShoppingCart = ShoppingCart.GetShoppingCart();
                cartTotal = usersShoppingCart.GetSubTotal();
                if (cartTotal > 0)
                {
                    BindData();
                    emptylbl.Visible = false;
                    btnUpdateCart.Visible = true;
                    CheckoutImageBtn.Visible = true;
                }
                else
                {
                    gvShoppingCart.Visible = false;

                    emptylbl.Visible = true;
                    btnUpdateCart.Visible = false;
                    CheckoutImageBtn.Visible = false;
                }
            }
        }
        protected void BindData()
        {

            DSSSite.App_Code.ShoppingCart cart = DSSSite.App_Code.ShoppingCart.GetShoppingCart();
            gvShoppingCart.DataSource = cart.Items;
            gvShoppingCart.DataBind();
        }
        protected void gvShoppingCart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // If we are binding the footer row, let's add in our total

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DSSSite.App_Code.ShoppingCart cart = DSSSite.App_Code.ShoppingCart.GetShoppingCart();
                e.Row.Cells[3].Text = "Total: " + cart.GetSubTotal().ToString("C");
            }
        }
        /**
        * This is the method that responds to the Remove button's click event
        */
        protected void gvShoppingCart_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                int productId = Convert.ToInt32(e.CommandArgument);
                DSSSite.App_Code.ShoppingCart cart = DSSSite.App_Code.ShoppingCart.GetShoppingCart();
                cart.RemoveItem(productId);
            }

            // We now have to re-setup the data so that the GridView doesn't keep
            // displaying the old data
            BindData();
        }

        protected void btnUpdateCart_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvShoppingCart.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    // We'll use a try catch block in case something other than a number is typed in
                    // If so, we'll just ignore it.
                    try
                    {
                        // Get the productId from the GridView's datakeys
                        int productId = Convert.ToInt32(gvShoppingCart.DataKeys[row.RowIndex].Value);
                        // Find the quantity TextBox and retrieve the value
                        int quantity = int.Parse(((TextBox)row.Cells[1].FindControl("txtQuantity")).Text);
                        DSSSite.App_Code.ShoppingCart cart = DSSSite.App_Code.ShoppingCart.GetShoppingCart();
                        cart.SetItemQuantity(productId, quantity);
                    }
                    catch (FormatException) { }
                }
            }

            BindData();
        }
        protected void CheckoutBtn_Click(object sender, ImageClickEventArgs e)
        {
            ShoppingCart usersShoppingCart = ShoppingCart.GetShoppingCart();
            Session["payment_amt"] = Decimal.Round(usersShoppingCart.GetSubTotal(), 2);
            
            Response.Redirect("Checkout/CheckoutStart.aspx");
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace DSSSite.Dashboard
{
    public partial class EditProduct : System.Web.UI.Page
    {
        public string query, constr;
        public SqlConnection con;
        public void connection()
        {
            constr = ConfigurationManager.ConnectionStrings["Imagecon"].ToString();
            con = new SqlConnection(constr);
            con.Open();


        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] != null)
            {


                if ((bool)Session["Login"].Equals(false))
                {
                    Response.Redirect("Login.aspx");
                }

            }
            else
                Response.Redirect("Login.aspx");
            
            if (!IsPostBack) //First page load
            {
                try
                {
                    DropDownList2.DataBind();
                    //productbindGrid();
                    showgrid();
                    //imagebindGrid();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else //Is Postback (Additional page loads)
            {
                
            }
        }
        public void imagebindGrid()
        {
            connection();
            query = "Select id, Image from ProductTable";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader dr = com.ExecuteReader();
            Gridview1.DataSource = dr;
            Gridview1.DataBind();
        }
        public void productbindGrid()
        {
            connection();
            query = "Select ID, [Product Name], [Product Description], [Product Price], [Weight], [Image] from ProductTable where [Product Type] = @Type";
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("@Type", SqlDbType.VarChar).Value = DropDownList2.SelectedValue;
            SqlDataReader dr = com.ExecuteReader();
            Gridview1.DataSource = dr;
            Gridview1.DataBind();
        }

        public void showgrid()
        {
            DataTable dt = new DataTable();
            connection();
            query = "Select ID, [Product Name], [Product Description], [Product Price], [Weight], [Image] from ProductTable where [Product Type] = @Type";
            SqlDataAdapter sda = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@Type", SqlDbType.VarChar).Value = DropDownList2.SelectedValue;
            cmd.CommandType = CommandType.Text;
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            Gridview1.DataSource = dt;
            Gridview1.DataBind();
        }
        
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
         {
          
           
        }
        /**
        * This is the method that responds to the Remove button's click event
        */
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                connection();
                query = "Delete from ProductTable where [ID] = @ID";
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@ID", SqlDbType.VarChar).Value = e.CommandArgument;
                SqlDataReader dr = com.ExecuteReader();
                showgrid();
            }
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Label lb = (Label)Gridview1.Rows[e.RowIndex].FindControl("Label6");
            TextBox tx1 = (TextBox)Gridview1.Rows[e.RowIndex].FindControl("Product_Name");
            TextBox tx2 = (TextBox)Gridview1.Rows[e.RowIndex].FindControl("Product_Description");
            TextBox tx3 = (TextBox)Gridview1.Rows[e.RowIndex].FindControl("Product_Price");
            TextBox tx4 = (TextBox)Gridview1.Rows[e.RowIndex].FindControl("Product_Weight");
            
             connection();
                query = "UPDATE ProductTable SET [Product Name]= @_Name, [Product Description] = @_Desc, [Product Price] = @_Price, [Weight] = @_Weight where [ID] = @ID";
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@ID", SqlDbType.VarChar).Value = lb.Text;
                com.Parameters.AddWithValue("@_Name", SqlDbType.VarChar).Value = tx1.Text;
                com.Parameters.AddWithValue("@_Desc", SqlDbType.VarChar).Value = tx2.Text;
                com.Parameters.AddWithValue("@_Price", SqlDbType.VarChar).Value = tx3.Text;
                com.Parameters.AddWithValue("@_Weight", SqlDbType.VarChar).Value = tx4.Text;
                Gridview1.EditIndex = -1;
                SqlDataReader dr = com.ExecuteReader();
                showgrid();
        }

        protected void GridView1_RowCancelingEdit
        (object sender, GridViewCancelEditEventArgs e)
        {
            Gridview1.EditIndex = -1;
            showgrid();
        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {

            Gridview1.EditIndex = e.NewEditIndex;
            showgrid();
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            showgrid();
        }
    }
}
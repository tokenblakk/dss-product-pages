﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProduct.aspx.cs" Inherits="DSSSite.AddProduct" %>

<!DOCTYPE html>

<html>
<head runat="server">
	<title>Dawn Simeon and Slicky's Honor</title>
	<link rel="stylesheet" type="text/css" href="../layout.css"/>
	<link rel="shortcut icon" href="../icon.ico"/>
</head>
<body>
<div id="container">
<div id="header">
	<span><a href="../#center">Skip to Content</a></span>
	<div class="hgroup">
<h1><img src="../pawprint.gif" height="50" width="50" alt=""/>Dawn Simeon And Slicky's Holistic Pet Remedies</h1>
	<h2>In Loving Hands...Healing is our most important product.</h2>
  </div>
	
</div>
<div id="content">
<div id="nav">
	<ul> 
        <li><a href="../index.html">Home</a></li>
		<li><a href="../products.aspx">Products</a></li>
        <li><a href="../courses.html">Dusky's Classes</a></li>
		<li><a href="../ailments.html">How to Treat an Ailment</a></li>
		<li><a href="../shelters.html">No Kill Animal Shelters</a></li>
		<li><a href="../chat">Chat with a Grief Coach</a></li>
		<li><a href="../news.html">Newsletter</a></li>
		<li><a href="../memoriam.html">Memoriam</a></li>
		<li><a href="../recommendations.html">Recommendations</a></li>
		<li><a href="../about.html">About The Company</a></li>
	</ul>
	<a href="Login.aspx" style="text-decoration:none;"><img src="../pawprint.gif" width="100" height="100" alt="Dawn Simeon and Slicky's Honor"/></a>
</div>


<div id="center">
    
    
<form id="form2" runat="server" 
    style="background-repeat: no-repeat;">
<div>
    <h2>Add New Product</h2>
    <a href="Home.aspx"><- Back to dashboard</a>
<table>
<tr>
<td>

    <br />
        <div class="type">
        Choose a Product Type to add:
        </div>
        <h2 class="type">
        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource5" DataTextField="Product_Type" DataValueField="Product_Type">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:Imagecon %>" SelectCommand="SELECT DISTINCT [Product Type] AS Product_Type FROM [ProductTable] ORDER BY [Product Type]"></asp:SqlDataSource>
    </h2>
    <asp:Label ID="Label5" runat="server" Text="Name of Product:"></asp:Label>&nbsp<asp:TextBox ID="TextBox1" runat="server" Width="231px"></asp:TextBox>
</td>
</tr>
<tr>
<td>
    <asp:Label ID="Label2" runat="server" Text="Product Description"></asp:Label>
    <div>
    <asp:TextBox ID="TextBox3" runat="server" Height="150px" Width="400px" MaxLength="500" Rows="5" Columns="1" TextMode="MultiLine">Enter a good explanation of the product here. (Max 500 Char.)</asp:TextBox>
        </div>

</td>
</tr>
    <tr>
<td>
    <asp:Label ID="Label3" runat="server" Text="Price: "></asp:Label>
    $<asp:TextBox ID="TextBox4" runat="server" Width="57px">1</asp:TextBox>
</td>
</tr>

<tr>
<td>
    <asp:Label ID="Label4" runat="server" Text="Weight (Oz.): "></asp:Label>
    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
    <br />
    <hr />
Select Image:&nbsp<asp:FileUpload ID="FileUpload1" runat="server" />
</td>
</tr>

</table>
<asp:Button ID="btnimguplod" runat="server" Text="Upload Product Details" onclick="upload" />
<br />
    <asp:Label ID="Label1" runat="server" Text="Label" ForeColor="#006600"></asp:Label>
</div>

<div>
<asp:GridView ID="Gridview1" CssClass="Gridview" runat="server" Width="500px"
HeaderStyle-BackColor="#7779AF" HeaderStyle-ForeColor="white">
<Columns>

<asp:TemplateField HeaderText="Image">
<ItemTemplate>

<asp:Image ID="Image1" runat="server" ImageUrl='<%# "../Handler1.ashx?id_Image="+ Eval("id") %>' CssClass="productImg"/>

</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>
    
</div>
</form>

<div id="footer">
	Copyright &copy; 2014 Dawn Simeon and Slicky's Honor Holistic Pet Remedies
</div>
</div>
    


</div>
</div>

</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DSSSite.Dashboard
{
    public partial class Home : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] != null)
            {


                if ((bool)Session["Login"].Equals(false))
                {
                    Response.Redirect("Login.aspx");
                }

            }
            else
                Response.Redirect("Login.aspx");
        }
    }
}
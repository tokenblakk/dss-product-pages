﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace DSSSite
{
    public partial class AddProduct : System.Web.UI.Page
    {

        public string query, constr;
        public SqlConnection con;
        public void connection()
        {
            constr =ConfigurationManager.ConnectionStrings["Imagecon"].ToString();
            con = new SqlConnection(constr);
            con.Open();
        
        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] != null)
            {


                if ((bool)Session["Login"].Equals(false))
                {
                    Response.Redirect("Login.aspx");
                }

            }
            else
                Response.Redirect("Login.aspx");

            Label1.Visible = false;
            if (!IsPostBack)
            {
                //imagebindGrid();
            
            }
        }


        protected void upload(object sender,EventArgs e)
        {
            ProductUpload();
        }


        private void ProductUpload()
        {
            if (FileUpload1.HasFile)
            {
                int imagefilelenth = FileUpload1.PostedFile.ContentLength;
                byte[] imgarray = new byte[imagefilelenth];
                HttpPostedFile image = FileUpload1.PostedFile;
                image.InputStream.Read(imgarray, 0, imagefilelenth);
                //Decimal _price = 0.00m;
                Decimal _price = Convert.ToDecimal(TextBox4.Text);
                //_price = Decimal.ToOACurrency(_price);


                connection();
                //query = "insert into ProductTable ([Product Name], [Product Description], [Product Type], [Image]) Values (@Name, @Desc, @Type, @Image)";
                query = "insert into ProductTable ([Product Name], [Product Description], [Product Price], [Product Type], [weight], [Image]) Values (@Name, @Desc, @Price, @Type, @Weight, @Image)";
                //query = "Update ProductTable Set Image = @Image, ProductName = @Name,  where ProductTable.id = @id";
                //query = "Insert into  ProductTable (Image) values (@Image) where ProductTable.id = @id";
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@Name", SqlDbType.VarChar).Value = TextBox1.Text;
                com.Parameters.AddWithValue("@Desc", SqlDbType.VarChar).Value = TextBox3.Text;
                com.Parameters.AddWithValue("@Price", SqlDbType.VarChar).Value = _price;
                com.Parameters.AddWithValue("@Type", SqlDbType.VarChar).Value = DropDownList2.SelectedValue;
                com.Parameters.AddWithValue("@Weight", SqlDbType.VarChar).Value = Convert.ToDecimal(TextBox5.Text);
                com.Parameters.AddWithValue("@Image", SqlDbType.Image).Value = imgarray;
                //com.Parameters.AddWithValue("@id", SqlDbType.Int).Value = TextBox2.Text;
                com.ExecuteNonQuery();
                Label1.Visible=true;
                Label1.Text = "Product Is Uploaded successfully.";
               
                //imagebindGrid();

            }

        }

        public void imagebindGrid()
        {
            connection();
            query = "Select id, Image from ProductTable";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader dr = com.ExecuteReader();
            Gridview1.DataSource = dr;
            Gridview1.DataBind();
        }

        protected void TextBox3_TextChanged(object sender, EventArgs e)
        {

        }

    }
}


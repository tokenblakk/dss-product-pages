﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace DSSSite.Dashboard
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        public string query, constr;
        public SqlConnection con;
        public void connection()
        {
            constr = ConfigurationManager.ConnectionStrings["Imagecon"].ToString();
            con = new SqlConnection(constr);
            con.Open();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] != null)
            {


                if ((bool)Session["Login"].Equals(false))
                {
                    Response.Redirect("Login.aspx");
                }

            }
            else
                Response.Redirect("Login.aspx");
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            //string connect = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=|DataDirectory|contacts.mdb";
            connection();
            query = "update Users set UserPassword = @_Pass Where Username = @User";
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("@User", Login1.UserName);
            com.Parameters.AddWithValue("@_Pass", Login1.Password);
            SqlDataReader dr = com.ExecuteReader();
            Response.Redirect("ChangePasswordSuccess.aspx");
        }
    }
}
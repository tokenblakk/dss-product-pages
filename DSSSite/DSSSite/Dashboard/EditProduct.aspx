﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditProduct.aspx.cs" Inherits="DSSSite.Dashboard.EditProduct" %>

<!DOCTYPE html>

<html>
<head runat="server">
	<title>Dawn Simeon and Slicky's Honor</title>
	<link rel="stylesheet" type="text/css" href="../layout.css"/>
	<link rel="shortcut icon" href="../icon.ico"/>
</head>
<body>
<div id="container">
<div id="header">
	<span><a href="../#center">Skip to Content</a></span>
	<div class="hgroup">
<h1><img src="../pawprint.gif" height="50" width="50" alt=""/>Dawn Simeon And Slicky's Holistic Pet Remedies</h1>
	<h2>In Loving Hands...Healing is our most important product.</h2>
  </div>
	
</div>
<div id="content">
<div id="nav">
	<ul> 
        <li><a href="../index.html">Home</a></li>
		<li><a href="../products.aspx">Products</a></li>
        <li><a href="../courses.html">Dusky's Classes</a></li>
		<li><a href="../ailments.html">How to Treat an Ailment</a></li>
		<li><a href="../shelters.html">No Kill Animal Shelters</a></li>
		<li><a href="../chat">Chat with a Grief Coach</a></li>
		<li><a href="../news.html">Newsletter</a></li>
		<li><a href="../memoriam.html">Memoriam</a></li>
		<li><a href="../recommendations.html">Recommendations</a></li>
		<li><a href="../about.html">About The Company</a></li>
	</ul>
<a href="Login.aspx" style="text-decoration:none;"><img src="../pawprint.gif" width="100" height="100" alt="Dawn Simeon and Slicky's Honor"/></a></div>


<div id="center">
    
    
<form id="form2" runat="server" 
    style="background-repeat: no-repeat;">
<div>
    <h2>Edit Product Details</h2>
    <a href="Home.aspx"><- Back to dashboard</a>
<table>
<tr>
<td>

    <br />
        <div class="type">
        Choose a Product Type to edit:
        </div>
        <h2 class="type">
        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource5" DataTextField="Product_Type" DataValueField="Product_Type" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:Imagecon %>" SelectCommand="SELECT DISTINCT [Product Type] AS Product_Type FROM [ProductTable] ORDER BY [Product Type]"></asp:SqlDataSource>
    </h2>
    &nbsp</td>
</tr>
    </table>

<div>
<asp:GridView ID="Gridview1" CssClass="Gridview" runat="server" Width="500px" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns ="false" onrowupdating="GridView1_RowUpdating" onrowcancelingedit="GridView1_RowCancelingEdit" 
        onrowediting="GridView1_RowEditing" HeaderStyle-BackColor="#7779AF" HeaderStyle-ForeColor="white" GridLines="None" CellPadding="2" CellSpacing="2" HorizontalAlign="Center">
<HeaderStyle BackColor="#7779AF" ForeColor="White"></HeaderStyle>

    <AlternatingRowStyle BackColor="White" />

    <Columns>

        
<asp:TemplateField HeaderText="ID Number">
    <ItemTemplate >
        <asp:Label ID="Label6" runat="server" Text='<%# Eval("ID") %>' >
</asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
<asp:TemplateField HeaderText="Image">
<ItemTemplate>

<asp:Image ID="Image1" runat="server" ImageUrl='<%# "../Handler1.ashx?id_Image="+ Eval("id") %>' CssClass="productImg"/><br />
    
    <br /><br />
    

</ItemTemplate>
</asp:TemplateField>


<asp:TemplateField HeaderText="Product Name">
     <ItemTemplate>
        <asp:Label runat="server" ID="Product_NameL" Columns="50" Text='<%# Eval("Product Name") %>'></asp:Label><br />
    </ItemTemplate>
    <EditItemTemplate>
        <asp:TextBox runat="server" ID="Product_Name" Columns="50" Text='<%# Eval("Product Name") %>'></asp:TextBox><br />
    </EditItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="Product Description">
         <ItemTemplate>
        <asp:Label runat="server" ID="Product_DescL" Columns="50" Text='<%# Eval("Product Description") %>'></asp:Label><br />
    </ItemTemplate>
    <EditItemTemplate>
        <asp:TextBox runat="server" ID="Product_Description" Columns="50" Text='<%# Eval("Product Description") %>'></asp:TextBox><br />
    </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Price">
         <ItemTemplate>
        $<asp:Label runat="server" ID="Product_PriceL" Columns="5" Text='<%# Eval("Product Price") %>'></asp:Label><br />
    </ItemTemplate>
    <EditItemTemplate>
        $<asp:TextBox runat="server" ID="Product_Price" Columns="5" Text='<%# Eval("Product Price") %>'></asp:TextBox><br />
    </EditItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Weight (Oz.)">
    <ItemTemplate>
        <asp:Label runat="server" ID="Product_WeightL" Columns="5" Text='<%# Eval("Weight") %>'></asp:Label> (Oz.)<br />
    </ItemTemplate>
    <EditItemTemplate>
        <asp:TextBox runat="server" ID="Product_Weight" Columns="5" Text='<%# Eval("Weight") %>'></asp:TextBox> (Oz.)<br />
    </EditItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="Edit" ShowHeader="false">
        <ItemTemplate>
            <asp:LinkButton ID="btnedit" runat="server" 
	CommandName="Edit" Text="Edit" ></asp:LinkButton>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:LinkButton ID="btnupdate" runat="server" 
	CommandName="Update" Text="Update" ></asp:LinkButton>
            <asp:LinkButton ID="btncancel" runat="server" 
	CommandName="Cancel" Text="Cancel"></asp:LinkButton>
            <br />
            <asp:LinkButton runat="server" ID="btnRemove" Text="Remove" CommandName="Remove" CommandArgument='<%# Eval("id") %>' style="font-size:12px;"></asp:LinkButton>
        </EditItemTemplate>
    </asp:TemplateField>
</Columns>
</asp:GridView>
    
    
</div>
</form>

<div id="footer">
	Copyright &copy; 2014 Dawn Simeon and Slicky's Honor Holistic Pet Remedies
</div>
</div>
    


</div>
</div>

</body>
</html>

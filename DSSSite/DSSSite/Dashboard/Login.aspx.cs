﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace DSSSite.Dashboard
{
    public partial class Login : System.Web.UI.Page
    {

        public string query, constr;
        public SqlConnection con;
        public void connection()
        {
            constr = ConfigurationManager.ConnectionStrings["Imagecon"].ToString();
            con = new SqlConnection(constr);
            con.Open();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] != null)
            {


                if ((bool)Session["Login"].Equals(true))
                {
                    Response.Redirect("Home.aspx");
                }

            }
            
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            //string connect = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=|DataDirectory|contacts.mdb";
            connection();
            query = "Select Count(*) From Users Where Username = @User And UserPassword = @Pass";
            SqlCommand com = new SqlCommand(query, con);
            int result = 0;
            
                com.Parameters.AddWithValue("@User", Login1.UserName);
                com.Parameters.AddWithValue("@Pass", Login1.Password);
                result = (Int32) com.ExecuteScalar();
                Session["User"] = Login1.UserName;
                
                
                //result =Convert.ToInt32(dr.ToString());

            if (result > 0)
            {
                Session["Login"] = true;
                Response.Redirect("Home.aspx");
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
}
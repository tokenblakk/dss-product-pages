﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSSSite.App_Code
{
    public class CartItem : IEquatable<CartItem>
    {
        public int Quantity { get; set; }
        private int _productID;
        public int ProductID
        {
            get { return _productID; }
            set
            {
                _product = null;
                _productID = value;
            }
        }

        private Product _product = null;
        public Product Prod
        {
            get
            {
                if (_product == null)
                {
                    _product = new Product(ProductID);
                }
                return _product;
            }
        }

        public string Description
        {
            get { return Prod.Description; }
        }
        public decimal Weight
        {
            get { return Prod.Weight * Quantity; }
        }
        public decimal UnitPrice
        {
            get { return Prod.Price; }
        }
        public decimal TotalPrice
        {
            get { return UnitPrice * Quantity; }
        }

        public CartItem(int productID)
        {
            this.ProductID = productID;
        }
        public bool Equals(CartItem item)
        {
            return item.ProductID == this.ProductID;
        }

    }
}
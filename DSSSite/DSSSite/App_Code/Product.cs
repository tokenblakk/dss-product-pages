﻿using System;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using DSSSite.App_Code;

namespace DSSSite.App_Code
{
    public class Product
    {
        public int ID { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public decimal Weight { get; set; }
        public string query, constr;
        public Int32 index;
        public SqlConnection con;

        public void connection()
        {
            constr = ConfigurationManager.ConnectionStrings["Imagecon"].ToString();
            con = new SqlConnection(constr);
            con.Open();
        }

        public Product(int id)
        {
            this.ID = id;
            setProduct(id);
        }
        private void GetProductData(SqlDataReader dr)
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    this.Description = dr.GetString(1);
                    decimal value = 0;
                    decimal _weight = 0;
                    //decimal.TryParse(Convert.ToString(dr[2]), out value);

                    value = Decimal.Round(dr.GetDecimal(2),2);
                    this.Price = value;
                    _weight= Decimal.Round(dr.GetDecimal(3), 2);
                    this.Weight = _weight;
                }

            }
        }

        private void setProduct(Int32 index)
        {
            connection();
            query = "Select ID, [Product Name], [Product Price], [Weight] from ProductTable where id=@id;";
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("@id", index);
            SqlDataReader dr = com.ExecuteReader();

            GetProductData(dr);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSSSite.App_Code
{
    public class ShoppingCart
    {
        public List<CartItem> Items { get; private set; }

        private const Decimal flatRate = 6.19m;
        private const Decimal magicNumber = 0.0842m;
        //public static readonly ShoppingCart Instance;

        // The static constructor is called as soon as the class is loaded into memory 
        public static ShoppingCart GetShoppingCart()
        {
            // If the cart is not in the session, create one and put it there
            if (HttpContext.Current.Session["ASPNETShoppingCart"] == null)
            {
                ShoppingCart cart = new ShoppingCart();
                cart.Items = new List<CartItem>();
                HttpContext.Current.Session["ASPNETShoppingCart"] = cart;
            }

            return (ShoppingCart)HttpContext.Current.Session["ASPNETShoppingCart"];
        }


        protected ShoppingCart() { }

        public void AddItem(int productID)
        {
            CartItem newItem = new CartItem(productID);

            if (Items.Contains(newItem))
            {
                foreach (CartItem item in Items)
                {
                    if (item.Equals(newItem))
                    {
                        item.Quantity++;
                        return;
                    }
                }
            }
            else
            {
                newItem.Quantity = 1;
                Items.Add(newItem);
            }
        }

        public void SetItemQuantity(int productID, int quantity)
        {
            if (quantity == 0)
            {
                RemoveItem(productID);
                return;
            }
            CartItem updatedItem = new CartItem(productID);
            foreach (CartItem item in Items)
            {
                if (item.Equals(updatedItem))
                {
                    item.Quantity = quantity;
                    return;
                }
            }
        }

        public void RemoveItem(int productID)
        {
            CartItem removedItem = new CartItem(productID);
            Items.Remove(removedItem);
        }

        public decimal GetSubTotal()
        {
            decimal subTotal = 0;
            foreach (CartItem item in Items)
                subTotal += item.TotalPrice;
            return subTotal;
        }
        public decimal GetTotalWeight()
        {
            decimal _weight = 0;
            foreach (CartItem item in Items)
                _weight += item.Weight;
            return _weight;
        }

        public decimal GetShipping()
        {
            Decimal _ship = GetTotalWeight();
            _ship = flatRate + (_ship * magicNumber);
            return _ship;

        }


        internal void EmptyCart()
        {
            Items.Clear();
        }
    }
}
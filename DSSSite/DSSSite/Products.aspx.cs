﻿using System;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using DSSSite.App_Code;


namespace DSSSite
{
    public partial class Default : System.Web.UI.Page
    {
        public string query, constr;
        public Int32 index;
        public SqlConnection con;
        public void connection()
        {
            constr = ConfigurationManager.ConnectionStrings["Imagecon"].ToString();
            con = new SqlConnection(constr);
            con.Open();


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    DropDownList2.DataBind();
                    DropDownList1.DataBind();
                    imagebindGrid();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else //Is Postback
            {
                imagebindGrid();
            }
        }


        public void imagebindGrid()
        {
            //DropDownList1.DataBind(); This breaks the list
            Gridview1.DataSource = null;
            Gridview1.DataBind();
            connection();
            query = "Select id, Image from ProductTable where id=@id;";
            index = Convert.ToInt32(DropDownList1.SelectedValue);
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("@id", index);
            SqlDataReader dr = com.ExecuteReader();
            Gridview1.DataSource = dr;
            Gridview1.DataBind();


        }

        protected void Gridview1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DropDownList1.DataBind();
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DropDownList1.DataBind();
            //imagebindGrid();
        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            imagebindGrid();
        }
        protected void btnGo_Click(object sender, EventArgs e)
        {
            DropDownList1.DataBind();
            imagebindGrid();
        }


        protected void btnAddtoCart_Click(object sender, EventArgs e)
        {
            Int32 index = Convert.ToInt32(DropDownList1.SelectedValue);
            DSSSite.App_Code.ShoppingCart cart = DSSSite.App_Code.ShoppingCart.GetShoppingCart();
            cart.AddItem(index);
            Response.Redirect("ViewCart.aspx");
        }

        protected void SqlDataSource5_Selecting(object sender, System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs e)
        {

        }

    }
}


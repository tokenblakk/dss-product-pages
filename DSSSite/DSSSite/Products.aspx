﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="DSSSite.Default" %>

<!DOCTYPE html>

<html>
<head runat="server">
	<title>Dawn Simeon and Slicky's Honor</title>
	<link rel="stylesheet" type="text/css" href="layout.css"/>
	<link rel="shortcut icon" href="icon.ico"/>
</head>
<body>
<div id="container">
<div id="header">
	<span><a href="#center">Skip to Content</a></span>
	<div class="hgroup">
<h1><img src="pawprint.gif" height="50" width="50" alt=""/>Dawn Simeon And Slicky's Holistic Pet Remedies</h1>
	<h2>In Loving Hands...Healing is our most important product.</h2>
  </div>
	
</div>
<div id="content">
<div id="nav">
	<ul>
        <li><a href="index.html">Home</a></li>
		<li><a href="products.aspx">Products</a></li>
        <li><a href="courses.html">Dusky's Classes</a></li>
		<li><a href="ailments.html">How to Treat an Ailment</a></li>
		<li><a href="shelters.html">No Kill Animal Shelters</a></li>
		<li><a href="chat">Chat with a Grief Coach</a></li>
		<li><a href="news.html">Newsletter</a></li>
		<li><a href="memoriam.html">Memoriam</a></li>
		<li><a href="recommendations.html">Recommendations</a></li>
		<li><a href="about.html">About The Company</a></li>
	</ul>
	<a href="Dashboard/Login.aspx" style="text-decoration:none;"><img src="pawprint.gif" width="100" height="100" alt="Dawn Simeon and Slicky's Honor"/></a>
    
</div>


<div id="center">
    

<audio id="audio" autoplay loop>
    <source src="resources/music/heal_the_world.mp3" type="audio/mp3"/>
	</audio>
<button onclick="document.getElementById('audio').muted = !document.getElementById('audio').muted">Toggle Music</button>

	<h2>Holistic Pet Foods and Remedies</h2>
	<p>We offer many different products and services to ensure your furry friend lives a happy and full life of love. </p>
	<p>We offer Nutritional Supplements, Flower Remedies and Topical Products with our partner, Azmira. Choose which holistic remedy is best for your pet's needs!</p>
	<hr>
	<h3><a href="productlist.html">Product Details and Full Price List</a></h3>
	<hr>
<div class="productspace">
<form id="form1" runat="server" 
    style="background-repeat: no-repeat;">
<div>
    <div class="type">
        Choose a Product Type:
        </div>
    <h2 class="type">
        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource5" DataTextField="Product_Type" DataValueField="Product_Type">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:Imagecon %>" SelectCommand="SELECT DISTINCT [Product Type] AS Product_Type FROM [ProductTable] ORDER BY [Product Type]"></asp:SqlDataSource>
        <asp:Button ID="btnGo" OnClick="btnGo_Click" runat="server" Text="Go" />
    </h2>
</div>
<div class="dropdown">

   
<table>
<tr>
<td>
    <span class="product">Choose a Product:</span><br />
&nbsp;<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="Product_Name" DataValueField="ID" CausesValidation="True">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Imagecon %>" SelectCommand="SELECT [ID], [Product Name] AS Product_Name FROM [ProductTable] WHERE ([Product Type] = @Product_Type)">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList2" Name="Product_Type" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</td>
</tr>
<tr>
<td>
    &nbsp;</td>
</tr>
</table>
</div>
    <div class="productData">
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CssClass="product" DataSourceID="SqlDataSource2" EmptyDataText="There are no data records to display." Height="50px" ShowHeader="False" GridLines="None">
        <Columns>
            <asp:BoundField DataField="Product_Name" HeaderText="Product_Name" SortExpression="Product_Name" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Imagecon %>" SelectCommand="SELECT [Product Name] AS Product_Name FROM [ProductTable] WHERE ([ID] = @ID)">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" Name="ID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" CssClass="price" DataSourceID="SqlDataSource4" EmptyDataText="There are no data records to display." Height="45px" ShowHeader="False" GridLines="None">
        <Columns>
            <asp:BoundField DataField="Product_Price" HeaderText="Product_Price" SortExpression="Product_Price" DataFormatString="{0:C}"/>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:Imagecon %>" SelectCommand="SELECT [Product Price] AS Product_Price FROM [ProductTable] WHERE ([ID] = @ID)">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" Name="ID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CssClass="productdescription" DataSourceID="SqlDataSource3" EmptyDataText="There are no data records to display." Min-Height="50px" ShowHeader="False" GridLines="None">
        <Columns>
            <asp:BoundField DataField="Product_Description" HeaderText="Product_Description" SortExpression="Product_Description" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:Imagecon %>" SelectCommand="SELECT [Product Description] AS Product_Description FROM [ProductTable] WHERE ([ID] = @ID)">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" Name="ID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    </div>

<div class="imgHolder">
<asp:GridView ID="Gridview1" CssClass="productImg" runat="server" 
HeaderStyle-BackColor="#7779AF" HeaderStyle-ForeColor="white" ShowHeader="False" AutoGenerateColumns="False" OnSelectedIndexChanged="Gridview1_SelectedIndexChanged" GridLines="None" HorizontalAlign="Center">
<Columns>

<asp:TemplateField HeaderText="Image" ShowHeader="False">
<ItemTemplate>

<asp:Image ID="Image1" runat="server" ImageUrl='<%# "Handler1.ashx?id_Image="+ Eval("id") %>' CssClass="productImg"/>

</ItemTemplate>
</asp:TemplateField>
</Columns>

<HeaderStyle BackColor="#7779AF" ForeColor="White"></HeaderStyle>
</asp:GridView>
</div>
    

<div class="cart">
    <div><asp:LinkButton runat="server" ID="btnAddtoCart" OnClick="btnAddtoCart_Click">Add To Cart</asp:LinkButton></div>
    <div><a href="ViewCart.aspx">View Cart</a></div>
</div>

    <div class="productblurb">
    
    Award Winning Quality and Satisfaction with Azmira food and remedies. Leading the way with sound clinical, double-blind studies in addition to feeding trials. For dogs, cats, ferrets, horses, birds, rabbits, pot belly pigs, etc... FOR ALL PETS
    </div>

</form>

    <div class="statement">
    Holistic Animal Care Recommendations, Diets, and
Supplements are not intended to be used in lieu of,
but as support to proper Veterinary Care and the
natural process of true healing.  These products are
not drugs and I am not making any drug claims. If
you suspect your pet has a medical condition, please
do seek out trusted veterinarian to assist you with
proper diagnosis and ongoing clinical verification of
whatever treatments or products you do choose, so that
they remain well suited for you pet's individual needs.
You can call me anytime for further information. These
statements have not been evaluated by the FDA.
</div>
</div>


<div id="footer">
	Copyright &copy; 2014 Dawn Simeon and Slicky's Honor Holistic Pet Remedies
</div>
</div>



</div>
</div>

</body>
</html>

﻿using System.Data.Entity;
using DSSSite.App_Code;
using DSSSite.Models;


namespace DSSSite.Models
{
  public class ProductContext : DbContext
  {
    public ProductContext()
      : base("DSSSite")
    {
    }
    //public DbSet<Category> Categories { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<CartItem> ShoppingCartItems { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<OrderDetail> OrderDetails { get; set; }
  }
}